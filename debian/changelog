ruby-devise (4.9.3-1) unstable; urgency=medium

  * New upstream version 4.9.3

 -- Pirate Praveen <praveen@debian.org>  Tue, 02 Jan 2024 18:32:52 +0530

ruby-devise (4.9.2-1) unstable; urgency=medium

  * Team upload.
  * New upstream version 4.9.2
  * d/control: Bump Standards-Version to 4.6.2.

 -- Aquila Macedo Costa <aquilamacedo@riseup.net>  Mon, 25 Sep 2023 16:57:08 -0300

ruby-devise (4.8.1-1) unstable; urgency=medium

  [ Debian Janitor ]
  * Team upload.
  * Bump debhelper from old 12 to 13.
  * Update standards version to 4.5.1, no changes needed.

  [ Mohammed Bilal ]
  * New upstream version 4.8.1
  * d/control: Bump Standards-Ver to 4.6.1
    -  Swtich to ruby:Depends for dependencies

 -- Mohammed Bilal <mdbilal@disroot.org>  Sat, 08 Oct 2022 21:51:10 +0530

ruby-devise (4.7.3-2) unstable; urgency=medium

  * Team upload.
  * d/t/smoke-test: skip javascript when creating a new app. For the purpose
    of this test the javascript assets are not needed.

 -- Lucas Kanashiro <kanashiro@debian.org>  Thu, 04 Mar 2021 09:58:29 -0300

ruby-devise (4.7.3-1) unstable; urgency=medium

  * Team upload

  [ Debian Janitor ]
  * Set field Upstream-Contact in debian/copyright.
  * Remove obsolete fields Contact, Name from debian/upstream/metadata (already
    present in machine-readable debian/copyright).

  [ Cédric Boutillier ]
  * Update team name
  * Add .gitattributes to keep unwanted files out of the source package

  [ Sruthi Chandran ]
  * New upstream version 4.7.3

 -- Sruthi Chandran <srud@debian.org>  Thu, 03 Dec 2020 15:46:12 +0530

ruby-devise (4.7.1-3) unstable; urgency=medium

  * Drop unnecessary build dependencies (Closes: #966938)

 -- Pirate Praveen <praveen@debian.org>  Thu, 06 Aug 2020 18:23:42 +0530

ruby-devise (4.7.1-2) unstable; urgency=medium

  * Reupload to unstable

 -- Pirate Praveen <praveen@debian.org>  Sun, 29 Mar 2020 05:33:56 +0530

ruby-devise (4.7.1-1) experimental; urgency=medium

  * Team upload
  * New upstream version 4.7.1

 -- Sruthi Chandran <srud@debian.org>  Tue, 04 Feb 2020 17:27:34 +0100

ruby-devise (4.6.2-2) unstable; urgency=medium

  [ Utkarsh Gupta ]
  * Add salsa-ci.yml
  * NOTE: 4.6.2-1 was uploaded to experimental and 4.5.0-3 to unstable.

  [ Pirate Praveen ]
  * Reupload 4.6.2 to unstable (gitlab in unstable cannot be supported
    meaningfully)
  * Bump Standards-Version to 4.4.1 (no changes needed)
  * Drop compat file, rely on debhelper-compat and bump compat level to 12
  * Remove obsolete needs-recommends field from debian/tests/control
  * Replace ADTTMP variable with AUTOPKGTEST_TMP in smoke-test

 -- Pirate Praveen <praveen@debian.org>  Tue, 19 Nov 2019 12:53:39 +0530

ruby-devise (4.5.0-3) unstable; urgency=medium

  * Team upload
  * Add patch to fix CVE-2019-5421 (Fixes: CVE-2019-5421) (Closes: #926348)

 -- Utkarsh Gupta <guptautkarsh2102@gmail.com>  Wed, 22 May 2019 00:38:15 +0530

ruby-devise (4.6.2-1) experimental; urgency=medium

  * Team upload

  [ Samyak Jain ]
  * Add d/upstream/metadata
  * Update d/control with epoch

  [ Utkarsh Gupta ]
  * New upstream version 4.6.2
  * Drop unwanted patches

 -- Utkarsh Gupta <guptautkarsh2102@gmail.com>  Tue, 21 May 2019 19:50:45 +0530

ruby-devise (4.5.0-2) unstable; urgency=medium

  * Team upload.
  * autopkgtest: make sure spring process is finished (Closes: #830092)

 -- Antonio Terceiro <terceiro@debian.org>  Wed, 27 Feb 2019 11:53:46 -0300

ruby-devise (4.5.0-1) unstable; urgency=medium

  * Team upload
  * New upstream version 4.5.0
  * Bump Standards-Version to 4.3.0
  * Update watch file

 -- Utkarsh Gupta <guptautkarsh2102@gmail.com>  Sun, 30 Dec 2018 02:55:33 +0530

ruby-devise (4.4.3-1) unstable; urgency=medium

  * New upstream version 4.4.3
  * Use salsa.debian.org in Vcs-* fields
  * Bump Standards-Version to 4.1.3 (no changes needed)
  * Bump debhelper compatibility level to 11

 -- Pirate Praveen <praveen@debian.org>  Wed, 21 Mar 2018 21:50:28 +0530

ruby-devise (4.3.0-1) unstable; urgency=medium

  * Team upload
  * New upstream release

 -- Sruthi Chandran <srud@disroot.org>  Fri, 15 Sep 2017 22:49:21 +0530

ruby-devise (4.2.0-1) unstable; urgency=medium

  * New upstream release

 -- Pirate Praveen <praveen@debian.org>  Sun, 18 Sep 2016 08:32:53 +0530

ruby-devise (4.1.1-4) unstable; urgency=medium

  * Remove debian/install (app directory should be installed as gem)
  * Install config to correct location

 -- Pirate Praveen <praveen@debian.org>  Tue, 13 Sep 2016 15:14:50 +0530

ruby-devise (4.1.1-3) unstable; urgency=medium

  * Use DH_RUBY = --gem-install
  * Refresh patches
  * Fix smoke-test

 -- Pirate Praveen <praveen@debian.org>  Fri, 26 Aug 2016 23:54:07 +0530

ruby-devise (4.1.1-2) unstable; urgency=medium

  * Reupload to unstable

 -- Pirate Praveen <praveen@debian.org>  Sat, 09 Jul 2016 19:49:45 +0530

ruby-devise (4.1.1-1) experimental; urgency=medium

  * New upstream release

 -- Pirate Praveen <praveen@debian.org>  Sun, 12 Jun 2016 13:11:23 +0530

ruby-devise (3.5.6-2) unstable; urgency=medium

  * Reupload to unstable

 -- Pirate Praveen <praveen@debian.org>  Fri, 04 Mar 2016 10:50:59 +0530

ruby-devise (3.5.6-1) experimental; urgency=medium

  * New upstream patch release
  * Bump standards version to 3.9.7
  * Switch vcs git url to https
  * Bump compat to 9

 -- Pirate Praveen <praveen@debian.org>  Tue, 23 Feb 2016 20:07:47 +0530

ruby-devise (3.5.2-3) unstable; urgency=medium

  * Install whole lib in package specific dir (Closes: #809560)

 -- Pirate Praveen <praveen@debian.org>  Sun, 03 Jan 2016 16:06:20 +0530

ruby-devise (3.5.2-2) unstable; urgency=medium

  * Team upload.
  * debian/patches/locales.patch: copy locales file from the right place
    (fixes rails generator)
  * Added DEP-8 (autopkgtest) smoke tests

 -- Antonio Terceiro <terceiro@debian.org>  Wed, 11 Nov 2015 15:40:56 -0200

ruby-devise (3.5.2-1) unstable; urgency=medium

  * New upstream release
  * Check gemspec dependencies during build

 -- Pirate Praveen <praveen@debian.org>  Fri, 30 Oct 2015 03:11:27 +0530

ruby-devise (3.5.1-1) unstable; urgency=medium

  * Team upload
  * Upstream update
  * Add dep3 header to patch
  * Fixed Vcs-* fields to follow new format

 -- Balasankar C <balasankarc@autistici.org>  Sat, 11 Jul 2015 10:55:20 +0530

ruby-devise (3.4.1-2) unstable; urgency=medium

  * Make it a proper rails engine (Closes: #782510)

 -- Pirate Praveen <praveen@debian.org>  Wed, 15 Apr 2015 20:04:57 +0530

ruby-devise (3.4.1-1) unstable; urgency=low

  [ Markus Tornow ]
  * Initial release (Closes: #691525)

 -- Pirate Praveen <praveen@debian.org>  Wed, 14 Jan 2015 12:08:03 +0530
